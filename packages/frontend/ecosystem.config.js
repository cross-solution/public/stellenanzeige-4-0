module.exports = {
  apps: [{
    name: '@yawik/stellenanzeige',
    script: 'yarn',
    interpreter: '/bin/bash',    
    args: 'start',
    env: {
      NODE_ENV: 'production',
      PORT: 5001
    },
  }],

  deploy: {
    production: {
      user: 'yawik',
      host: 'api.yawik.org',
      ref: 'origin/main',
      repo: 'https://gitlab.com/cross-solution/public/stellenanzeige-4-0.git',
      path: '/home/yawik/stellenanzeige-4-0',      
      'pre-deploy-local': 'rsync -a --exclude=node_modules --delete dist/spa/ yawik@api.yawik.org:/home/yawik/stellenanzeige-4-0/source/packages/frontend/dist/spa/',
      'post-deploy': 'cd /home/yawik/stellenanzeige-4-0/source/packages/frontend/ && pm2 startOrRestart ecosystem.config.js --interpreter bash --env production',
      'pre-setup': 'pm2 ps'
    }
  }
};

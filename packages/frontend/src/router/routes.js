
const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('pages/Home.vue'),
  },
  {
    path: '/:lang',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: 'success',
        name: 'submitSuccessful',
        component: () => import('pages/SubmitSuccessful.vue')
      },
      {
        path: 'auth',
        name: 'auth',
        component: () => import('pages/Auth.vue'),
      },
      {
        path: 'register',
        name: 'register',
        component: () => import('pages/Auth.vue'),
      },
      {
        path: 'auth/admin',
        name: 'admin-auth',
        component: () => import('pages/Auth.vue'),
      },
      {
        // must be after /success - otherwise :jobid will match "success"
        path: ':title?',
        name: 'job',
        props: true,
        component: () => import('pages/PageApply.vue')
      },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
];

export default routes;

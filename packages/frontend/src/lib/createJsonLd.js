import JobPosting from 'src/assets/JobPosting.json';
import general from 'src/data/general.json';

function addDays(date, days)
{
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
};

function createJsonLd(job)
{
  const today = new Date();
  const expires = addDays(today, 25);
  const o = JobPosting;

  o.title = job.title;
  o.description = '<p>' +
    general.description + '</p><p><b>' +
    (job.tasks.label
      ? job.tasks.label
      : 'Aufgaben') + '</b></p><ul><li>' +
    job.tasks.items.join('</li><li>') + '</li></ul><p><br><b>' +
    (job.profile.label
      ? job.profile.label
      : 'Profil') + '</b></p><ul><li>' +
    job.profile.items.join('</li><li>') + '</li></ul><br><p><b>' +
    (general.benefits.label
      ? general.benefits.label
      : 'Benefits') + '</b></p><ul><li>' +
    general.benefits.items.map(v => v.desc).join('</li><li>') + '</li></ul>';
  o.identifier.name = 'ghg';
  o.identifier.value = job.ref;
  o.datePosted = today.toISOString().slice(0, 10);
  o.validThrough = expires.toISOString().slice(0, 10);
  o.employmentType = 'FULL_TIME';
  o.directApply = true;

  return JSON.stringify(o);
}

export default createJsonLd;

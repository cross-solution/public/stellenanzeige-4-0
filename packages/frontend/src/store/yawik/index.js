import {
  GET_FIRST_NAME,
  GET_LAST_NAME,
  GET_PHONE,
  GET_EMAIL,
  GET_STATS,
  GET_TOKEN,
  HAS_AUTH,
  SET_FIRST_NAME,
  SET_LAST_NAME,
  SET_PHONE,
  SET_EMAIL,
  SET_DATE_CREATED,
  SET_DATE_MODIFIED,
  SET_UUID,
  SET_PLATFORM,
  SET_TOKEN
} from '../names';

export default
{
  state()
  {
    return {
      form:
      {
        firstName: '',
        lastName: '',
        phone: '',
        email: '',
      },
      stats: {
        uuid: null,
        dateCreated: null,
        dateModified: null,
        platform: {}
      },
      token: null,
    };
  },
  getters:
    {
      [GET_FIRST_NAME](state)
      {
        return state.form.firstName;
      },
      [GET_LAST_NAME](state)
      {
        return state.form.lastName;
      },
      [GET_PHONE](state)
      {
        return state.form.phone;
      },
      [GET_EMAIL](state)
      {
        return state.form.email;
      },
      [GET_STATS](state)
      {
        return state.stats;
      },
      [GET_TOKEN](state)
      {
        return state.token;
      },
      [HAS_AUTH](state)
      {
        return state.token && state.token !== null;
      },
    },
  mutations:
    {
      [SET_FIRST_NAME](state, value)
      {
        state.form.firstName = value;
      },
      [SET_LAST_NAME](state, value)
      {
        state.form.lastName = value;
      },
      [SET_PHONE](state, value)
      {
        state.form.phone = value;
      },
      [SET_EMAIL](state, value)
      {
        state.form.email = value;
      },
      [SET_UUID](state, value)
      {
        state.stats.uuid = value;
      },
      [SET_DATE_CREATED](state, value)
      {
        state.stats.dateCreated = value;
      },
      [SET_DATE_MODIFIED](state, value)
      {
        state.stats.dateModified = value;
      },
      [SET_PLATFORM](state, value)
      {
        state.stats.platform = value;
      },
      [SET_TOKEN](state, token)
      {
        state.token = token;
      },
    }
};

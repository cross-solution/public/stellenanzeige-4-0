// ./src/api/[api-name]/content-types/restaurant/lifecycles.js

module.exports = {
  async beforeCreate(data) {
    console.log("### beforeCreate Hook: data:", data)
  },
  async afterCreate(event) {
    const { result, params } = event;
    
    const _finalRes = await strapi.plugins['email'].services.email.send({
          to: 'bewerbung@ghgnetz.de',
          subject: 'Bewerbung als ' + params.data.job_title,
          text: 'test',
          html: '<p>Neue Bewerbungs als <b>' + params.data.job_title + '</b></p>' +
                '<p>Kandidate: ' + params.data.firstname + ' ' + params.data.lastname + '</p>' +
                '<p>Antworten:</p>' + 
                '<table>' + params.data.answers.map(x => '<tr><td>' + x.a + '</td><td>' + x.q +'</td></tr>').join('') + 
                '</table>' +
                '<p>Telefon: <a href="tel:' + params.data.phone + '">' + params.data.phone + '</a></p>' +
                '<p>Email: <a href="mailto:' + params.data.email + '">' + params.data.email + '</a></p>'
        });
        
    console.log("### params: ", params)
    console.log("### afterCreate Hook: data:", _finalRes)

    // do something to the result;
  },
};

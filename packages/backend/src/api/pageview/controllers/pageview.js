'use strict';

/**
 *  pageview controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::pageview.pageview');

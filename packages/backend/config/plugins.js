module.exports = ({ env }) => ({
  email: {
    config: {
      provider: 'nodemailer',
      providerOptions: {
        host: env('EMAIL_SMTP_HOST', 'mc.cross-solution.de'), //SMTP Host
        port: env('EMAIL_SMTP_PORT', 465), //SMTP Port
        secure: env('EMAIL_SMTP_SECURE', true),
        auth: {
          user: env('EMAIL_SMTP_USERNAME', ''),
          pass: env('EMAIL_SMTP_PASSWORD', ''),
        },
        rejectUnauthorized: true,
        requireTLS: true,
      },
      settings: {
        defaultFrom: env('EMAIL_SETTINGS_FROM', ''),
        defaultReplyTo: env('EMAIL_SETTINGS_REPLYTO', '')
      },
      
    },
  },
  deepl: {
    enabled: true,
    config: {
      // your DeepL API key
      apiKey: 'key',
      // whether to use the free or paid api, default true
      freeApi: true,
      // Which field types are translated (default string, text, richtext, components and dynamiczones)
      translatedFieldTypes: [
        'string',
        'text',
        'richtext',
        'component',
        'dynamiczone',
      ],
      // If relations should be translated (default true)
      translateRelations: true,
      // You can define a custom glossary to be used here (see https://www.deepl.com/docs-api/managing-glossaries/)
      // glossaryId: 'customGlossary',
    },
  },
});

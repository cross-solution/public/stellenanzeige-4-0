# Stellenanzeige 4.0

Die Stellenanzeige 4.0 ist eine Stellenanzeige in Form einer App. Als App
kann eine Stellenanzeige plötzlich viele Dinge.

- Statistiken. Die Stellenanzeige sammelt eigenständig die Zahlen Besucher, Seitenaufrufe und Klick auf den Bewerbungslink
- In der Stellenanzeige ist ein Bewerbungsformular integriert.
- Die Stellenanzeige funktioniert besser auf dem Handy.
- Die Stellenanzeige enthält alles, was für die Veröffentlichung auf Google Jobs benötigt wird.

## Anforderungen

- yarn
- node >=16

## Installation

```
git clone https://gitlab.com/g-h-g/page.git
cd page
yarn && yarn lerna bootstrap
```
## Los geht's

das Backend kann man starten per:

```
yarn start:backend
```

Strapi erreicht man dann: http://127.0.0.1:1337


das Frontend kann man sterten per:

```
yarn start:frontend
```

Die Stellenanzeige erreicht man: http://127.0.0.1:8080

